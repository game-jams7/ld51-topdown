﻿using Assets.Scripts.Player.Weapons;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] Camera camera;
        CharacterMovement movement;
        CabinController cabin;
        PrimaryWeapon primary;

        public void Initialize(CharacterMovement movement, CabinController cabin, PrimaryWeapon primary)
        {
            this.movement = movement;
            this.cabin = cabin;
            this.primary = primary;
        }

        void FixedUpdate()
        {
            var movementVector = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
            if(movementVector.magnitude > 0.01f)
                movement.MoveBy(movementVector);

            cabin.AimAt(GetTargetPoint());
        }

        void Update()
        {
            if (Input.GetButton("Fire1"))
                primary.Fire(GetTargetPoint());

            if(Input.GetButton("Boost"))
                movement.BoostOn();

            if (Input.GetButtonUp("Boost"))
                movement.BoostOff();
        }

        Vector3 GetTargetPoint()
        {
            Vector3 targetPoint = Vector3.zero;
            var ray = camera.ScreenPointToRay(Input.mousePosition);
            var fPlane = new Plane(Vector3.up, Vector3.zero);
            float distance;
            if (fPlane.Raycast(ray, out distance))
                targetPoint = ray.GetPoint(distance);

            return targetPoint;
        }
    }
}
