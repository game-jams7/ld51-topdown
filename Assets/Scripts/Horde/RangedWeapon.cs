﻿using System.Collections;
using Assets.Scripts.Player.Weapons;
using UnityEngine;

namespace Assets.Scripts.Horde
{
    public class RangedWeapon : HordeWeapon
    {
        [SerializeField] Shot shotPrefab;
        [SerializeField] float minEngageDistance;

        bool IsInRange => Vector3.Distance(target.position, transform.position) < minEngageDistance;

        public override void Initialize(Transform target)
        {
            this.target = target;
            StartCoroutine(WaitAndAttack());
        }

        IEnumerator WaitAndAttack()
        {
            yield return new WaitForSeconds(attackRate);
            Attack();
            StartCoroutine(WaitAndAttack());
        }

        void Attack()
        {
            if (IsInRange)
            {
                attackVfx.Play();
                var shot = Instantiate(shotPrefab, transform.position, Quaternion.identity);
                shot.Initialize(1, target.position);
            }
        }
    }
}
