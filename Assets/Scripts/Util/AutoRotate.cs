﻿using UnityEngine;

namespace Assets.Scripts.Util
{
    public class AutoRotate : MonoBehaviour
    {
        [SerializeField] float speed;
        [SerializeField] Vector3 axis;

        void Update()
        {
            transform.RotateAround(transform.position, axis, speed * Time.fixedDeltaTime);
        }
    }
}
