﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class XpTracker : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI text;

        public void UpdateXp(int current, int max) => 
            text.text = $"XP: {current}/{max}";
    }
}