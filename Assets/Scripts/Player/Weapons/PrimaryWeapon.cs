﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Player.Weapons
{

    public class PrimaryWeapon : MonoBehaviour
    {
        [SerializeField] Shot shotPrefab;
        [SerializeField] FiringPosition[] firingPositions;
        [SerializeField] Vector3 aimOffset;

        int damage;
        int currentBarrelIndex = 0;
        Action onAmmoSpent;
        CharacterStats stats;

        int currentAmmo;
        bool isCycling;

        bool CanFire => currentAmmo > 0 & !isCycling;

        public void Fire(Vector3 targetPoint)
        {
            if (CanFire)
            {
                firingPositions[currentBarrelIndex].Fire(shotPrefab, damage, targetPoint + aimOffset);
                currentBarrelIndex++;
                if (currentBarrelIndex >= firingPositions.Length)
                    currentBarrelIndex = 0;

                currentAmmo--;
                onAmmoSpent();
                isCycling = true;
                StartCoroutine(Cycle());
            }
        }

        IEnumerator Cycle()
        {
            yield return new WaitForSeconds(stats.RateOfFire);
            isCycling = false;
        }

        public void Initialize(CharacterStats stats, Action onAmmoSpent)
        {
            this.stats = stats;
            this.onAmmoSpent = onAmmoSpent;

            damage = stats.Damage;
            currentAmmo = stats.Ammo;
        }

        public void Reload() => 
            currentAmmo = stats.Ammo;
    }
}
