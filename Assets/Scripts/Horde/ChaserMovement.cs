﻿using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Horde
{
    public class ChaserMovement : HordeMovement
    {
        void Update()
        {
            if (target)
            {
                if(agent.isOnNavMesh)
                    agent.SetDestination(target.position);
                else
                    transform.position = Vector3.zero;
            }
        }

    }
}
