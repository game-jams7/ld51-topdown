﻿using System;
using Assets.Scripts.Player;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class UpgradeMenuItem : MonoBehaviour
    {
        [SerializeField] Image buttonImage;
        [SerializeField] Button button;
        [SerializeField] TextMeshProUGUI tooltip;

        UpgradeType upgradeType;

        public void LoadUpgrade(StatUpgrade upgradeInfo)
        {
            upgradeType = upgradeInfo.UpgradeType;
            tooltip.text = upgradeInfo.Tooltip;
            buttonImage.sprite = upgradeInfo.Icon;
        }

        public void Initialize(Action<UpgradeType> onClick) => 
            button.onClick.AddListener(() => onClick(upgradeType));
    }
}
