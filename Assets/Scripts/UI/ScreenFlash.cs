﻿using UnityEngine;

namespace Assets.Scripts.UI
{
    public class ScreenFlash : MonoBehaviour
    {
        [SerializeField] Animator animator;
        public void OnHurt() => animator.SetTrigger("FlashHurt");

        public void OnRefresh() => animator.SetTrigger("FlashRefresh");
    }
}