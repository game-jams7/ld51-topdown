﻿using System;
using Assets.Scripts.Player.Weapons;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class CabinController : MonoBehaviour
    {
        [SerializeField] Vector3 aimOffset;

        void NormalizeRoll()
        {
            var euler = transform.eulerAngles;
            euler.z = 0;
            transform.eulerAngles = euler;
        }

        public void AimAt(Vector3 targetPoint)
        {
            transform.LookAt(targetPoint + aimOffset);
            NormalizeRoll();
        }
    }
}
