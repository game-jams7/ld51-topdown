﻿using UnityEngine;

namespace Assets.Scripts.Util
{
    public class AutoDispose : MonoBehaviour
    {
        [SerializeField] float life;

        void Start() => 
            Destroy(gameObject, life);
    }
}
