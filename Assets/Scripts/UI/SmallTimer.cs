﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class SmallTimer : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI counter;
        [SerializeField] Image radial;

        int interval;
        float remaining;
        Action onIntervalElapsed;

        public void Initialize(int interval, Action onIntervalElapsed)
        {
            this.onIntervalElapsed = onIntervalElapsed;
            this.interval = interval;
            Reset();
        }

        void FixedUpdate()
        {
            remaining -= Time.fixedDeltaTime;
            counter.text = Mathf.RoundToInt(remaining).ToString("0");
            radial.fillAmount = remaining / interval;

            if (remaining <= 0)
            {
                onIntervalElapsed();
                Reset();
            }
        }

        void Reset() => 
            remaining = interval;
    }
}
