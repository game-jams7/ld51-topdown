﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Horde
{
    public class HordeUnit : MonoBehaviour
    {
        [SerializeField] int baseHitPoints;
        [SerializeField] int baseXpValue;
        [SerializeField] LevelScaling scaling;

        [SerializeField] HordeMovement movement;
        [SerializeField] HordeWeapon weapon;

        [SerializeField] ParticleSystem deathExplosion;
        [SerializeField] AudioSource explosionSfx;
        [SerializeField] AudioSource impactSfx;

        [SerializeField] GameObject view;
        [SerializeField] Collider collider;

        int currentHP;
        Action<int> onKilled;
        int hitPoints;
        int xpValue;
        public bool IsPooled { get; private set; }

        public void Initialize(Transform parent, Transform target, Action<int> onKilled, int level)
        {
            ApplyLevelScaling(level);
            IsPooled = false;
            this.onKilled = onKilled;
            currentHP = hitPoints;
            transform.SetParent(parent);
            movement.Initialize(target);
            weapon.Initialize(target);
        }

        public void Respawn(Vector3 spawnPosition, int level)
        {
            ApplyLevelScaling(level);
            transform.position = spawnPosition;
            IsPooled = false;
            currentHP = hitPoints;
            view.SetActive(true);
            movement.Activate();
            weapon.Activate();
            collider.enabled = true;
        }

        void ApplyLevelScaling(int level)
        {
            hitPoints += baseHitPoints + (int)(scaling.HpFactor) * level;
            xpValue += baseXpValue + (int)(scaling.XpFactor) * level;
            transform.localScale = Vector3.one + (Vector3.one * scaling.SizeFactor * level);
        }

        public void ReceiveDamage(int damage)
        {
            currentHP -= damage;
            impactSfx.Play();
            if (currentHP <= 0)
                KillUnit();
        }

        void KillUnit()
        {
            view.SetActive(false);
            movement.Deactivate();
            weapon.Deactivate();
            deathExplosion.Play();
            explosionSfx.Play();
            onKilled(xpValue);
            collider.enabled = false;
            StartCoroutine(WaitAndPool());
        }

        IEnumerator WaitAndPool()
        {
            yield return new WaitForSeconds(3f);
            IsPooled = true;
        }
    }

    [Serializable]
    public class LevelScaling
    {
        public float SizeFactor;
        public float HpFactor;
        public float XpFactor;
    }
}
