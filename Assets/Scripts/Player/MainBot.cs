﻿using Assets.Scripts.Player.Weapons;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class MainBot : MonoBehaviour
    {
        [SerializeField] Health health;
        [SerializeField] AudioSource reloadSfx;

        CharacterStats stats;
        CharacterMovement movement;
        Hud hud;
        PrimaryWeapon primary;

        public void Initialize(CharacterStats stats, CharacterMovement movement, PrimaryWeapon primary, Hud hud)
        {
            Time.timeScale = 1;
            this.movement = movement;
            this.primary = primary;
            this.hud = hud;

            this.stats = stats;
            movement.Initialize(stats, hud.OnBoostSpent);
            primary.Initialize(stats, hud.OnAmmoSpent);
            hud.Initialize(stats, RefreshAll, EndGame);
            health.Initialize(stats, hud, EndGame);
        }

        void EndGame()
        {
            Time.timeScale = 0;
            hud.OnEndGame();
        }

        void RefreshAll()
        {
            movement.RechargeBoost();
            primary.Reload();
            hud.Reset();
            reloadSfx.Play();
        }

        public void Heal(int amount) => 
            health.Heal(amount);

        public void UpdateStats()
        {
            hud.Remap(stats);
            Debug.Log(stats.ToString());
        }
    }
}
