using UnityEngine;

namespace Assets.Scripts.Visual
{
    public class Anchor : MonoBehaviour
    {
        [SerializeField] Transform anchor;
        [SerializeField] float elasticity;

        void LateUpdate() => 
            transform.position = Vector3.Lerp(transform.position, anchor.position, elasticity);
    }
}
