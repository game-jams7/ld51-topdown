﻿using UnityEngine;

namespace Assets.Scripts.Player.Weapons
{
    public class FiringPosition : MonoBehaviour
    {
        [SerializeField] ParticleSystem muzzleFlash;
        [SerializeField] AudioSource fireSfx;

        public void Fire(Shot shotPrefab, int damage, Vector3 targetPoint)
        {
            muzzleFlash.Play();
            fireSfx.Play();
            var shot = Instantiate(shotPrefab, transform.position, transform.rotation);
            shot.Initialize(damage, targetPoint);
        }
    }
}