﻿using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Horde
{
    public abstract class HordeMovement : MonoBehaviour
    {
        [SerializeField] protected float speed;
        [SerializeField] protected NavMeshAgent agent;

        protected Transform target;

        public void Initialize(Transform target)
        {
            this.target = target;
            agent.speed = speed;
        }

        public void Deactivate()
        {
            enabled = false;
            agent.enabled = false;
        }

        public void Activate()
        {
            enabled = true;
            agent.enabled = true;
        }
    }
}