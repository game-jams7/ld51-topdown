﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Util;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Horde
{
    public class HordeSpawner : MonoBehaviour
    {
        [SerializeField] Transform targetTransform;
        [SerializeField] HordeUnit[] unitPrefabs;
        [SerializeField] float spawnRadius;
        [SerializeField] float spawnTime;
        [SerializeField] float difficultyScaleTime;
        [SerializeField] float difficultyModifier;

        List<HordeUnit> units;
        Action<int> onUnitKilled;
        int maxUnitLevel = 1;

        public void Initialize(Action<int> onUnitKilled)
        {
            units = new List<HordeUnit>();
            this.onUnitKilled = onUnitKilled;
            StartCoroutine(WaitAndSpawn());
            StartCoroutine(WaitAndRaiseDifficulty());
        }

        IEnumerator WaitAndSpawn()
        {
            SpawnUnit();
            yield return new WaitForSeconds(spawnTime);
            StartCoroutine(WaitAndSpawn());
        }

        IEnumerator WaitAndRaiseDifficulty()
        {
            yield return new WaitForSeconds(difficultyScaleTime);
            maxUnitLevel++;
            spawnTime -= spawnTime * difficultyModifier;
            StartCoroutine(WaitAndRaiseDifficulty());
        }

        void SpawnUnit()
        {
            var randomCircle = Random.onUnitSphere;
            var spawnPosition = targetTransform.position + new Vector3(randomCircle.x, 0f, randomCircle.z) * spawnRadius;
            var level = Random.Range(0, maxUnitLevel);

            if (units.Any(u => u.IsPooled))
            {
                units.First(u => u.IsPooled).Respawn(spawnPosition, level);
            }
            else
            {
                var unit = Instantiate(unitPrefabs.PickOne(), spawnPosition, Quaternion.identity);
                unit.Initialize(transform, targetTransform, onUnitKilled, level);
                units.Add(unit);
            }
        }
    }
}
