﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Player;
using UnityEngine;

namespace Assets.Scripts.Horde
{
    public class MeleeWeapon : HordeWeapon
    {
        [SerializeField] float attackRange;
        [SerializeField] LayerMask targetLayer;

        public override void Initialize(Transform _)
        {
            StartCoroutine(WaitAndAttack());
        }

        void Update()=> Debug.DrawLine(transform.position, transform.position + transform.forward * attackRange, Color.red);

        IEnumerator WaitAndAttack()
        {
            yield return new WaitForSeconds(attackRate);
            Attack();
            StartCoroutine(WaitAndAttack());
        }

        void Attack()
        {
            attackVfx.Play();
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, attackRange, targetLayer))
                hit.collider.GetComponent<Health>().ReceiveDamage();
        }
    }
}
