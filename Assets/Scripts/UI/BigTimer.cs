﻿using System;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace Assets.Scripts.UI
{
    public class BigTimer : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI clock;

        TimeSpan remainingTime;
        Action onEndGame;

        public void Initialize(int levelLength, Action onEndGame)
        {
            this.onEndGame = onEndGame;
            remainingTime = TimeSpan.FromMinutes(levelLength);
        }

        void FixedUpdate()
        {
            remainingTime = remainingTime.Subtract(TimeSpan.FromSeconds(Time.deltaTime));
            clock.text = remainingTime.ToString(@"mm\:ss");
            if (remainingTime.TotalSeconds <= 0)
                onEndGame();
        }
    }
}
