﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.UI;
using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class XpManager : MonoBehaviour
    {
        [SerializeField] StatUpgradeConfiguration upgradeConfiguration;
        [SerializeField] int firstLevelXp;

        CharacterStats stats;
        MainBot bot;
        Hud hud;
        int currentXp;
        int nextLevelXp;

        public void Initialize(CharacterStats stats, MainBot bot, Hud hud)
        {
            nextLevelXp = firstLevelXp;
            this.hud = hud;
            this.bot = bot;
            this.stats = stats;
            hud.InitializeUpgradePanel(UpgradeStat);
        }

        List<StatUpgrade> UpgradeList() => new List<StatUpgrade>
        {
            upgradeConfiguration.DamageSet,
            upgradeConfiguration.AmmoSet,
            upgradeConfiguration.BlockSet,
            upgradeConfiguration.BoostSet,
            upgradeConfiguration.SpeedSet,
            upgradeConfiguration.RateOfFireSet,
            upgradeConfiguration.HpSet,
            upgradeConfiguration.HealSet
        };

        public void GainXp(int amount)
        {
            currentXp += amount;
            if (currentXp >= nextLevelXp)
                LevelUp();

            hud.UpdateXp(currentXp, nextLevelXp);
        }

        void LevelUp()
        {
            Time.timeScale = 0;
            ShowUpgrades();
            nextLevelXp += nextLevelXp + (int) (nextLevelXp * 0.05f);
        }

        void ShowUpgrades()
        {
            var candidates = UpgradeList();
            var upgrades = new List<StatUpgrade>();
            foreach (var _ in Enumerable.Range(0, 3))
            {
                var upgrade = candidates.PickOne();
                upgrades.Add(upgrade);
                candidates.Remove(upgrade);
            }

            hud.ShowUpgradePanel(upgrades);
        }

        void UpgradeStat(UpgradeType upgradeType)
        {
            switch (upgradeType)
            {
                case UpgradeType.Ammo:
                    stats.Ammo += Mathf.RoundToInt(stats.Ammo * upgradeConfiguration.AmmoSet.Amount);
                    break;
                case UpgradeType.RateOfFire:
                    stats.RateOfFire -= stats.RateOfFire * upgradeConfiguration.RateOfFireSet.Amount;
                    break;
                case UpgradeType.Damage:
                    stats.Damage += Mathf.RoundToInt(stats.Damage * upgradeConfiguration.DamageSet.Amount);
                    break;
                case UpgradeType.Speed:
                    stats.WalkSpeed += stats.WalkSpeed * upgradeConfiguration.SpeedSet.Amount;
                    stats.BoostSpeed += stats.BoostSpeed * upgradeConfiguration.SpeedSet.Amount;
                    break;
                case UpgradeType.Boost:
                    stats.Boost += stats.Boost * upgradeConfiguration.BoostSet.Amount;
                    break;
                case UpgradeType.BlockChance:
                    stats.BlockChance += stats.BlockChance * upgradeConfiguration.BlockSet.Amount;
                    break;
                case UpgradeType.Hp:
                    stats.HitPoints += upgradeConfiguration.HpSet.Amount;
                    break;
                case UpgradeType.Heal:
                    bot.Heal(upgradeConfiguration.HealSet.Amount);
                    break;
            }

            Time.timeScale = 1;
            bot.UpdateStats();
        }
    }
}
