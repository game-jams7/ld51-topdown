﻿using System;
using System.Collections;
using Assets.Scripts.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Player
{
    public class Health : MonoBehaviour
    {
        [SerializeField] float protectionTime;
        CharacterStats stats;
        Hud hud;

        int currentHp;
        bool isProtected;
        Action onEndGame;

        public void Initialize(CharacterStats stats, Hud hud, Action onEndGame)
        {
            this.onEndGame = onEndGame;
            currentHp = stats.HitPoints;
            this.stats = stats;
            this.hud = hud;
        }

        public void ReceiveDamage()
        {
            if (!isProtected)
            {
                var blockAttempt = Random.value;
                if (blockAttempt > stats.BlockChance)
                {
                    currentHp--;
                    hud.OnHurt();
                    if (currentHp <= 0)
                        onEndGame();
                }

                StartCoroutine(Protect());
            }
        }

        IEnumerator Protect()
        {
            isProtected = true;
            yield return new WaitForSeconds(protectionTime);
            isProtected = false;
        }

        public void Heal(int amount)
        {
            currentHp += amount;
            if (currentHp > stats.HitPoints)
                currentHp = stats.HitPoints;

            hud.OnHeal();
        }
    }
}
