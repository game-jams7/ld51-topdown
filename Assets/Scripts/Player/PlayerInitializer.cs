﻿using System;
using Assets.Scripts.Player.Weapons;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerInitializer : MonoBehaviour
    {
        [SerializeField] CharacterStats initialStats;
        [SerializeField] CabinController cabin;
        [SerializeField] PrimaryWeapon primary;
        [SerializeField] CharacterMovement movement;
        [SerializeField] PlayerInput input;
        [SerializeField] MainBot mainBot;
        [SerializeField] Hud hud;
        [SerializeField] XpManager xpManager;

        CharacterStats realStats;

        public Action<int> OnEnemyKilled => xpManager.GainXp;
        
        public void Initialize()
        {
            realStats = initialStats.MakeCopy();

            mainBot.Initialize(realStats, movement, primary, hud);
            input.Initialize(movement, cabin, primary);
            xpManager.Initialize(realStats, mainBot, hud);
        }
    }
}
