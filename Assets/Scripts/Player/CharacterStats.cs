﻿using UnityEngine;

namespace Assets.Scripts.Player
{
    [CreateAssetMenu(fileName = "PlayerStats", menuName = "Configuration/PlayerStats")]
    public class CharacterStats : ScriptableObject
    {
        public int HitPoints;
        public float BlockChance;

        public int Ammo;
        public int Damage;
        public float RateOfFire;
        
        public float WalkSpeed;
        public float BoostSpeed;
        public float Boost;

        public CharacterStats MakeCopy()
        {
            return new CharacterStats()
            {
                HitPoints = this.HitPoints,
                BlockChance = this.BlockChance,
                Ammo = this.Ammo,
                Damage = this.Damage,
                RateOfFire = this.RateOfFire,
                WalkSpeed = this.WalkSpeed,
                BoostSpeed = this.BoostSpeed,
                Boost = this.Boost
            };
        }

        public override string ToString() => 
            $"Ammo: {Ammo}, Damage: {Damage}, Rate of Fire: {RateOfFire}, Hit Points: {HitPoints},  Speed: {WalkSpeed}/{BoostSpeed}, Boost: {Boost}, Block: {BlockChance}";
    }
}
