﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Player
{
    public class CharacterMovement : MonoBehaviour
    {
        [SerializeField] NavMeshAgent agent;
        [SerializeField] ParticleSystem boostVfx;
        [SerializeField] AudioSource boostSfx;

        float walkSpeed;
        float boostSpeed;
        Action<float> onBoostSpent;
        bool boosting;
        float remainingBoost;
        CharacterStats stats;

        public void Initialize(CharacterStats stats, Action<float> onBoostSpent)
        {
            this.stats = stats;
            walkSpeed = stats.WalkSpeed;
            boostSpeed = stats.BoostSpeed;
            this.onBoostSpent = onBoostSpent;
            remainingBoost = stats.Boost;

            agent.speed = walkSpeed;
        }

        public void BoostOn()
        {
            if (!boosting && remainingBoost > 0)
            {
                boosting = true;
                boostVfx.Play();
                agent.speed = boostSpeed;
                boostSfx.Play();
            }
        }

        public void BoostOff()
        {
            boosting = false;
            boostVfx.Stop();
            agent.speed = walkSpeed;
            boostSfx.Stop();
        }

        public void MoveBy(Vector3 movementVector)
        {
            agent.SetDestination(transform.position + movementVector);
            if (boosting)
            {
                remainingBoost -= Time.fixedDeltaTime;
                onBoostSpent(remainingBoost);
                if(remainingBoost <= 0)
                    BoostOff();
            }
        }

        public void RechargeBoost() => 
            remainingBoost = stats.Boost;
    }
}
