﻿using UnityEngine;

namespace Assets.Scripts.Horde
{
    public abstract class HordeWeapon : MonoBehaviour
    {
        [SerializeField] protected float attackRate;
        [SerializeField] protected ParticleSystem attackVfx;

        protected Transform target;

        public virtual void Initialize(Transform target) => 
            this.target = target;

        public void Deactivate() => StopAllCoroutines();

        public void Activate() => Initialize(target);
    }
}