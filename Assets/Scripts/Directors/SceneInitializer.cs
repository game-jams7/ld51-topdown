using Assets.Scripts.Horde;
using Assets.Scripts.Player;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Directors
{
    public class SceneInitializer : MonoBehaviour
    {
        [SerializeField] Texture2D cursorTexture;
        [SerializeField] PlayerInitializer player;
        [SerializeField] HordeSpawner horde;
        [SerializeField] Hud hud;

        [SerializeField] CharacterStats initialStats;
        [SerializeField] LevelConfiguration levelSettings;

        void Start()
        {
            hud.LoadLevelSettings(levelSettings);
            InitializeCursor();
            player.Initialize();
            horde.Initialize(player.OnEnemyKilled);
        }

        void InitializeCursor()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
            var hotspot = new Vector2(cursorTexture.width / 2f, cursorTexture.height / 2f);
            Cursor.SetCursor(cursorTexture, hotspot, CursorMode.Auto);
        }
    }
}
