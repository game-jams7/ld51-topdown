﻿using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Horde
{
    public class StalkerMovement : HordeMovement
    {
        [SerializeField] float distance;

        void Update()
        {
            if (target)
            {
                if(agent.isOnNavMesh)
                    agent.SetDestination(target.position + (transform.position - target.position).normalized * distance);
                else
                    transform.position = Vector3.zero;
            }
        }

    }
}
