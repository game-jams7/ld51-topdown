﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.MainMenu
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] Button start;
        [SerializeField] Button exit;

        void Start()
        {
            start.onClick.AddListener(() => SceneManager.LoadScene("InGame"));
            exit.onClick.AddListener(Application.Quit);
        }
    }
}
