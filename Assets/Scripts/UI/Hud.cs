﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Player;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class Hud : MonoBehaviour
    {
        [SerializeField] PipCounter ammoCounter;
        [SerializeField] PipCounter hpCounter;
        [SerializeField] BarCounter boostCounter;
        [SerializeField] SmallTimer intervalTimer;
        [SerializeField] BigTimer levelTimer;
        [SerializeField] XpTracker xpTracker;
        [SerializeField] UpgradePanel upgradePanel;
        [SerializeField] EndMenu endPanel;
        
        [SerializeField] ScreenFlash screenFlash;
        LevelConfiguration settings;
        
        public void LoadLevelSettings(LevelConfiguration settings) => 
            this.settings = settings;

        public void Initialize(CharacterStats stats, Action onIntervalElapsed, Action onEndGame)
        {
            ammoCounter.Initialize(stats.Ammo);
            hpCounter.Initialize(stats.HitPoints);
            boostCounter.Initialize(stats.Boost);
            levelTimer.Initialize(settings.LevelLengthMinutes, onEndGame);
            intervalTimer.Initialize(settings.IntervalSeconds, onIntervalElapsed);
            endPanel.Initialize();
        }

        public void InitializeUpgradePanel(Action<UpgradeType> onUpgrade) => upgradePanel.Initialize(onUpgrade);

        public void OnBoostSpent(float amount) => 
            boostCounter.Spend(amount);

        public void OnAmmoSpent() =>
            ammoCounter.Spend();

        public void OnHurt()
        {
            screenFlash.OnHurt();
            hpCounter.Spend();
        }

        public void OnEndGame() => 
            endPanel.Open();

        public void Reset()
        {
            ammoCounter.Reset();
            boostCounter.Reset();
            screenFlash.OnRefresh();
        }

        public void UpdateXp(int current, int max) => 
            xpTracker.UpdateXp(current, max);

        public void Remap(CharacterStats stats)
        {
            ammoCounter.Remap(stats.Ammo);
            hpCounter.Remap(stats.HitPoints);
            boostCounter.Remap(stats.Boost);
        }

        public void ShowUpgradePanel(IEnumerable<StatUpgrade> upgrades) => 
            upgradePanel.Load(upgrades.ToArray());

        public void OnHeal()
        {
            hpCounter.OnHeal();
        }
    }
}
