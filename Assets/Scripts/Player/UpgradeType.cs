﻿namespace Assets.Scripts.Player
{
    public enum UpgradeType
    {
        None,
        Ammo,
        RateOfFire,
        Damage,
        Speed,
        Boost,
        Hp,
        Heal,
        BlockChance
    }
}