﻿using System;
using UnityEngine;

namespace Assets.Scripts.Player
{
    [CreateAssetMenu(fileName = "StatUpgrades", menuName = "Configuration/StatUpgrades")]
    public class StatUpgradeConfiguration : ScriptableObject
    {
        public StatUpgradePercentile AmmoSet;
        public StatUpgradePercentile RateOfFireSet;
        public StatUpgradePercentile DamageSet;
        public StatUpgradePercentile SpeedSet;
        public StatUpgradePercentile BoostSet;
        public StatUpgradeInteger HpSet;
        public StatUpgradeInteger HealSet;
        public StatUpgradePercentile BlockSet;
    }

    public abstract class StatUpgrade
    {
        public string Tooltip;
        public Sprite Icon;
        public UpgradeType UpgradeType;
    }

    [Serializable]
    public class StatUpgradePercentile : StatUpgrade
    {
        public float Amount;
    }

    [Serializable]
    public class StatUpgradeInteger : StatUpgrade
    {
        public int Amount;
    }
}