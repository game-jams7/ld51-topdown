﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class PipCounter : MonoBehaviour
    {
        [SerializeField] Pip pipPrefab;

        List<Pip> pips;

        public void Initialize(int initialAmount)
        {
            pips = new List<Pip>();
            foreach (var _ in Enumerable.Range(0, initialAmount))
            {
                var pip = Instantiate(pipPrefab, transform);
                pip.Reset();
                pips.Add(pip);
            }
        }

        public void Spend() => 
            pips.Last(p => !p.IsSpent).Spend();

        public void Reset()
        {
            foreach (var p in pips)
                p.Reset();
        }

        public void Remap(int newAmmo)
        {
            var amountToAdd = newAmmo - pips.Count;
            foreach (var _ in Enumerable.Range(0, amountToAdd))
            {
                var pip = Instantiate(pipPrefab, transform);
                pip.Spend();
                pips.Add(pip);
            }
        }

        public void OnHeal() => 
            pips.First(p => p.IsSpent).Reset();
    }
}
