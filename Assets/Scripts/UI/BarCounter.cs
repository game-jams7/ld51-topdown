﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class BarCounter : MonoBehaviour
    {
        [SerializeField] Image bar;

        float maxAmount;

        public void Initialize(float initialAmount) => 
            maxAmount = initialAmount;

        public void Spend(float amount) => 
            bar.fillAmount = amount / maxAmount;

        public void Reset() => 
            bar.fillAmount = 1;

        public void Remap(in float newAmount) => 
            maxAmount = newAmount;
    }
}
