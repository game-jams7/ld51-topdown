﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class Pip : MonoBehaviour
    {
        [SerializeField] Image image;
        [SerializeField] float spentAlpha;

        public bool IsSpent { get; private set; }

        public void Spend()
        {
            IsSpent = true;
            var color = image.color;
            color.a = spentAlpha;
            image.color = color;
        }

        public void Reset()
        {
            IsSpent = false;
            var color = image.color;
            color.a = 1;
            image.color = color;
        }
    }
}
