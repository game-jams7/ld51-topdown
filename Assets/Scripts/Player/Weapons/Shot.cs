﻿using Assets.Scripts.Horde;
using UnityEngine;

namespace Assets.Scripts.Player.Weapons
{
    public class Shot : MonoBehaviour
    {
        [SerializeField] float speed;
        [SerializeField] float lifeTime;
        [SerializeField] LayerMask enemyLayer;
        [SerializeField] LayerMask sceneryLayer;
        [SerializeField] GameObject holePrefab;

        int damage;

        public void Initialize(int damage, Vector3 targetPoint)
        {
            this.damage = damage;
            transform.LookAt(targetPoint);
            Destroy(gameObject, lifeTime);
        }

        void FixedUpdate()
        {
            var frameSpeed = speed * Time.fixedDeltaTime;
            transform.Translate(Vector3.forward * frameSpeed);
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, frameSpeed, enemyLayer))
            {
                hit.collider.GetComponent<HordeUnit>()?.ReceiveDamage(damage);
                hit.collider.GetComponent<Health>()?.ReceiveDamage();
                Instantiate(holePrefab, hit.point, Quaternion.identity);
                Destroy(gameObject);
            }
            else if (Physics.Raycast(transform.position, transform.forward, out hit, frameSpeed, sceneryLayer))
            {
                Instantiate(holePrefab, hit.point, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
