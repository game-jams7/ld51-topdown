﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class EndMenu : MonoBehaviour
    {
        [SerializeField] Button retry;
        [SerializeField] Button exit;

        public void Initialize()
        {
            retry.onClick.AddListener(() => SceneManager.LoadScene("InGame"));
            exit.onClick.AddListener(() => SceneManager.LoadScene("Menu"));
        }

        public void Open() => 
            gameObject.SetActive(true);
    }
}
