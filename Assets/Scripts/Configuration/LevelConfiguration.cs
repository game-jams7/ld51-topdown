﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    [CreateAssetMenu(fileName = "LevelConfiguration", menuName = "Configuration/Level")]
    public class LevelConfiguration : ScriptableObject
    {
        public int IntervalSeconds;
        public int LevelLengthMinutes;
    }
}
