﻿using System;
using Assets.Scripts.Player;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class UpgradePanel : MonoBehaviour
    {
        [SerializeField] UpgradeMenuItem[] upgradeSelectors;
        [SerializeField] Animator animator;
        Action<UpgradeType> onUpgradeSelected;

        public void Initialize(Action<UpgradeType> onUpgradeSelected)
        {
            this.onUpgradeSelected = onUpgradeSelected;
            foreach (var selector in upgradeSelectors)
                selector.Initialize(OnUpgrade);
        }

        public void Load(StatUpgrade[] upgrades)
        {
            gameObject.SetActive(true);
            animator.SetTrigger("Open");
            for (int i = 0; i < upgrades.Length; i++)
            {
                upgradeSelectors[i].LoadUpgrade(upgrades[i]);
            }
        }

        void OnUpgrade(UpgradeType selection)
        {
            animator.SetTrigger("Close");
            onUpgradeSelected(selection);
            Invoke("Deactivate", 1f);
        }

        void Deactivate() => gameObject.SetActive(false);
    }
}
